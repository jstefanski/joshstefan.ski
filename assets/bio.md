# Why hello there!

I am a largely self-taught programmer after discovering QBasic on my first computer at a young age.  I've dabbled in a variety of languages over the years and even co-founded an internet services company before deciding to persue a career as a game developer.

I've been working on games professionally for the past 14+ years and I been fortunate enough to work on some well-known and amazing projects with some amazing people.

I love working on game technology and have found myself focusing on tools and developer productivity lately.  I have also spent some time teaching programming and game development.