# joshstefan.ski

This is the source for the site https://joshstefan.ski/

Yes, this is over-engineered for such a simple site, but I am using this to gain more experience in various technologies (Vue, Nuxt, GitLab CI, etc.) in an environment that is not mission critical. :)